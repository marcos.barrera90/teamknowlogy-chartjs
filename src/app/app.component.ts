import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { Chart, ChartDataSets, ChartData, ChartOptions } from 'chart.js';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  @ViewChild('grafica', { static: true }) graficaREF: ElementRef;

  ngOnInit(){

    // CHARTDATASET CONFIGURATION
    const datasets:ChartDataSets[] = [
      {
        label: 'Fan page',
        fill: false,
        data: [292.67, 292.82, 292.54, 292.54, 292.95, 292.41, 292.68, 292.27, 292.41, 292.21, 292.37, 292.50],
        backgroundColor: '#4C4CD8',
        borderColor: '#4C4CD8',
        borderWidth: 1,
        yAxisID: 'Fan page',
        pointBackgroundColor: '#4C4CD8',
        pointHoverRadius: 10,
        pointHoverBackgroundColor: '#4C4CD8a1',
        pointHoverBorderColor: '#4c4cd8a1',
        pointHoverBorderWidth: 12
      },
      {
        fill: false,
        data: [292.67, 292.82, 292.54, 292.54, 292.95, 292.41, 292.68, 292.27, 292.41, 292.21, 292.37, 292.50],
        backgroundColor: '#4C4CD8',
        yAxisID: 'Fan page',
        pointHoverBorderWidth: 0,
        pointHoverBackgroundColor: '#4C4CD8',
        radius: 4,
      },
      {
        label: 'Engaged users',
        fill: false,
        data: [21.43, 21.43, 26.78, 16.07, 16.07, 10.71, 10.71, 21.43, 5.36, 10.71, 5.36, 10],
        backgroundColor: '#F8CB1C',
        borderColor: '#F8CB1C',
        borderWidth: 1,
        yAxisID: 'Engaged users',
        pointBackgroundColor: '#F8CB1C',
        pointHoverRadius: 10,
        pointHoverBackgroundColor: '#F8CB1C85',
        pointHoverBorderColor: '#f8cb1c85',
        pointHoverBorderWidth: 12
      },
      {
        fill: false,
        data: [21.43, 21.43, 26.78, 16.07, 16.07, 10.71, 10.71, 21.43, 5.36, 10.71, 5.36, 10],
        backgroundColor: '#F8CB1C',
        yAxisID: 'Engaged users',
        pointHoverBorderWidth: 0,
        pointHoverBackgroundColor: '#F8CB1C',
        radius: 4,
      }
    ]

    // CHARTDATA
    const data: ChartData = {
      labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dic'],
      datasets
    }

    // CHARTOPTIONS
    const options: ChartOptions = {
      responsive: false,
      legend: {
        display: false
      },
      hover: {
        mode: 'x',
        intersect: false
      },
      scales: {
        xAxes: [
          {
            ticks: {
              fontColor: '#EBEBEB',
              callback: (value) => {
                return value
              },
              fontSize: 10,
              major: {
                enabled: true,
                fontColor: '#F8CB1C',
              },
              fontFamily: 'Work Sans',
              padding: 6
            },
            gridLines: {
              display: false,
            },
          },

        ],
        yAxes: [{
          id: 'Fan page',
          position: 'left',
          ticks: {
            fontColor: '#EBEBEB',
            padding: 15,
            fontSize: 10,
            major: {
              enabled: true,
              fontColor: '#4C4CD8',
            },
            fontFamily: 'Work Sans',
            stepSize: 0.14,
            callback: (value: string, index, values) => {
              const newValue = parseFloat(value).toFixed(2)
              return newValue + ' K'
            }
          },
          gridLines: {
            borderDash: [1, 2],
            color: "#282828",
            drawBorder: false,
            tickMarkLength: 0,
          }
        },
        {
          id: 'Engaged users',
          position: 'right',
          ticks: {
            fontColor: '#B8B7B7',
            fontSize: 10,
            major: {
              enabled: true,
              fontColor: '#F8CB1C',
            },
            padding: 15,
            fontFamily: 'Work Sans',
            stepSize: 4,
            callback: (value: string, index, values) => {
              const newValue = parseFloat(value).toFixed(2)
              return newValue
            },
          },
          gridLines: {
            drawBorder: false,
            tickMarkLength: 0,
            color: 'transparent'
          }
        }]
      },
      tooltips: {
        enabled: false,
        mode: 'x',
        intersect: false
      },
      onHover: function (evt: any, element: any) {
        if (element.length) {
          const yRight = myChart.data.datasets[0].data[element[0]._index];
          // Find the closest number in an array 
          const closestRight = myChart.chart.scales['Fan page'].ticksAsNumbers.reduce((a, b) => {
            return Math.abs(b - yRight) < Math.abs(a - yRight) ? b : a;
          });
          const indexRight = myChart.chart.scales['Fan page'].ticksAsNumbers.indexOf(closestRight);

          const yLeft = myChart.data.datasets[2].data[element[0]._index];
          // Find the closest number in an array 
          const closestLeft = myChart.chart.scales['Engaged users'].ticksAsNumbers.reduce((a, b) => {
            return Math.abs(b - yLeft) < Math.abs(a - yLeft) ? b : a;
          });
          const indexLeft = myChart.chart.scales['Engaged users'].ticksAsNumbers.indexOf(closestLeft);

          myChart.update();
          myChart.chart.scales['x-axis-0']._ticks[element[0]._index].major = true;
          myChart.chart.scales['Fan page']._ticks[indexRight].major = true;
          myChart.chart.scales['Engaged users']._ticks[indexLeft].major = true;
        }
      }
    }


    // INIT GRAPH
    const myChart = new Chart('grafica', {
      type: 'line',
      data,
      options,
    })
  }

}

